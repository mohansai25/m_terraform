resource "aws_eip" "My_elasticIP" {
tags = {
  Name= "MYEIP"
}
}

resource "aws_nat_gateway" "My_nat" {
  depends_on = [aws_eip.My_elasticIP]
  subnet_id = aws_subnet.public.id
  allocation_id = aws_eip.My_elasticIP.id
tags = {
    Name="My_Nat"
  }

}