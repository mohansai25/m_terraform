resource "aws_eks_cluster" "my_eks" {
  name     = "New_eks_cluster1"
  role_arn = aws_iam_role.eks_ClusterRole.arn
  #version  = "1.21"
  #enabled_cluster_log_types = ["audit","api"] # enable for logs

  vpc_config {
    security_group_ids = ["sg-0a7f6ae0a47eb8221"]

    subnet_ids              = ["subnet-05d212405802857db","subnet-0fef03430c795fa12","subnet-01a5d3ea3a91132b0"]
    endpoint_private_access = true
    endpoint_public_access  = true
    public_access_cidrs     = ["0.0.0.0/0"]
  }
      tags = {
        Name = "new_eks_cluster"
      }
        depends_on = [
    aws_iam_role_policy_attachment.cluster-AmazonEKSClusterPolicy,
    aws_iam_role_policy_attachment.cluster-AmazonEKSServicePolicy,
    #aws_cloudwatch_log_group.Cloud_Log # Enable  aws_cloudwatch_log_group resource for logs

  ]
}

resource "aws_iam_role" "eks_ClusterRole" {
  name = "eks-cluster-example"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "cluster-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_ClusterRole.name
}
resource "aws_iam_role_policy_attachment" "cluster-AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.eks_ClusterRole.name
}

#resource "aws_cloudwatch_log_group" "Cloud_Log" {
#  #name              = "/aws/eks/${var.cluster_name}/cluster"
#  name              = "/aws/eks/New_eks_cluster1/cluster"
#  retention_in_days = 7
#  tags = {
#    Environment = "production"
#    Application = "serviceA"
#  }
#}