### EKS Node Groups
#resource "aws_eks_node_group" "this" {
#  cluster_name    = aws_eks_cluster.my_eks.name
#  node_group_name = "Test_Node_Group"
#  node_role_arn   = aws_iam_role.node.arn
#  subnet_ids      = ["subnet-05d212405802857db","subnet-0fef03430c795fa12","subnet-01a5d3ea3a91132b0"]
#
#  scaling_config {
#    desired_size =2
#    max_size     = 5
#    min_size     = 1
#  }
#
#  #ami_type       = "AL2_x86_64" # AL2_x86_64, AL2_x86_64_GPU, AL2_ARM_64, CUSTOM
#  #capacity_type  = "ON_DEMAND"  # ON_DEMAND, SPOT
#  #disk_size      = 20
#  instance_types = ["t2.medium"]
#
#
#  depends_on = [
#    aws_iam_role_policy_attachment.node_AmazonEKSWorkerNodePolicy,
#    aws_iam_role_policy_attachment.node_AmazonEKS_CNI_Policy,
#    aws_iam_role_policy_attachment.node_AmazonEC2ContainerRegistryReadOnly,
#  ]
#}
#
#
## EKS Node IAM Role
#resource "aws_iam_role" "node" {
#  name = "Worker-Role"
#
#  assume_role_policy = <<POLICY
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Effect": "Allow",
#      "Principal": {
#        "Service": "ec2.amazonaws.com"
#      },
#      "Action": "sts:AssumeRole"
#    }
#  ]
#}
#POLICY
#}
#
#resource "aws_iam_role_policy_attachment" "node_AmazonEKSWorkerNodePolicy" {
#  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
#  role       = aws_iam_role.node.name
#}
#
#resource "aws_iam_role_policy_attachment" "node_AmazonEKS_CNI_Policy" {
#  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
#  role       = aws_iam_role.node.name
#}
#
#resource "aws_iam_role_policy_attachment" "node_AmazonEC2ContainerRegistryReadOnly" {
#  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
#  role       = aws_iam_role.node.name
#}
#
