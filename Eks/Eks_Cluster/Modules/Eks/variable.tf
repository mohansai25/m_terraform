variable "subnet_ids"{}
variable "sg" {}
variable "instance_size"{}
variable "scaling_config_MAX_size"{}
variable "scaling_config_MIN_size"{}
variable "scaling_config_desired_size"{}
variable "aws_eks_cluster_name"{}
variable "node_group_name"{}