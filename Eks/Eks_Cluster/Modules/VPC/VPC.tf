resource "aws_vpc" "My_VPC" {

  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags = {
    Name="${var.vpc_name}"
  }
}
