resource "aws_subnet" "public" {
  depends_on = [aws_vpc.My_VPC]
  vpc_id = aws_vpc.My_VPC.id
  cidr_block = "${var.public_subnet_CIDR}"
  map_public_ip_on_launch = "true"
  #availability_zone = ""
  tags = {
   Name = "${var.public_subnet_Name}"
  }
}

resource "aws_subnet" "private" {
  count =length(var.pri_subnet_CIDR)
  #depends_on = [aws_vpc.My_VPC]
  vpc_id = aws_vpc.My_VPC.id
  availability_zone = element(var.pri_azs,count.index )
  cidr_block = element(var.pri_subnet_CIDR,count.index )
  map_public_ip_on_launch = "true"
  #availability_zone = ""
  tags = {
   Name = "Private_${count.index+1}"
  }
}
