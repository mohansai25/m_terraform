variable "subnet_ids" {}
variable "sg" {}
variable "instance_size"{}
variable "scaling_config_MAX_size"{}
variable "scaling_config_MIN_size"{}
variable "scaling_config_desired_size"{}
variable "aws_eks_cluster_name"{}
variable "node_group_name"{}

#variable "vpc_cidr" {}
#variable "vpc_name" {}
#
#variable "public_subnet_CIDR"  {}
#variable "public_subnet_Name"{}
#
#variable "pri_subnet_CIDR"  {}
#variable "pri_azs"  {}
#subnet_id=["subnet-05d212405802857db","subnet-0fef03430c795fa12","subnet-01a5d3ea3a91132b0"]
  #sgw=sg-02db00db71e98e6a3""