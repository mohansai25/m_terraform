subnet_ids =["subnet-0c977718413c30d62","subnet-031839be98228295b","subnet-08180f36b1348d5b2"]
sg = "sg-09cf67d96cc4e46fc"
#instance_size="t2.medium"
instance_size="t2.small"
scaling_config_MAX_size=5
scaling_config_MIN_size=2
scaling_config_desired_size=3
aws_eks_cluster_name= "Prod-Cluster"
node_group_name="worker"

#vpc_cidr = "10.61.0.0/16"
#vpc_name="ReadhotVPC"
#
#private_subnet_CIDR="10.61.1.0/24"
#private_subnet_Name="Private"
#
#public_subnet_CIDR="10.61.2.0/24"
#public_subnet_Name="Public"