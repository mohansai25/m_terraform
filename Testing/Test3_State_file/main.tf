provider "aws" {
  #access_key = "${var.aws_access_key}"
  #secret_key = "${var.aws_secret_key}"
  region = "${var.aws_region}"

}

resource "aws_s3_bucket" "my_bucket" {
  bucket = "tesingbucket15"
  tags = {
    Name= "terraform_logs"
    Env ="Dev"
  }
}
resource "aws_s3_bucket_acl" "bucketacl" {
  depends_on = [aws_s3_bucket.my_bucket]
  bucket = aws_s3_bucket.my_bucket.id
  acl="private"
}

resource "aws_vpc" "My_VPC" {

  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags = {
    Name="${var.vpc_name}"
  }
}
resource "aws_flow_log" "vpc_flow_log" {
  depends_on = [aws_s3_bucket.my_bucket]
  log_destination = aws_s3_bucket.my_bucket.arn
  log_destination_type = "s3"
  traffic_type = "ALL"
  vpc_id = aws_vpc.My_VPC.id
  tags = {
    Name="All VPC_LOGS"
  }

}

resource "aws_subnet" "private" {
  vpc_id = aws_vpc.My_VPC.id
  cidr_block = "${var.private_subnet_CIDR}"
  availability_zone = ""
  tags = {
    Name = "${var.private_subnet_Name}"
  }
}

resource "aws_subnet" "public" {
  vpc_id = aws_vpc.My_VPC.id
  map_public_ip_on_launch = true
  cidr_block = "${var.public_subnet_CIDR}"
  tags = {
    Name = "${var.public_subnet_Name}"
  }
}
#
#
resource "aws_internet_gateway" "My_internet" {
  vpc_id = aws_vpc.My_VPC.id
  tags = {
    Name="Test"
  }
}
resource "aws_eip" "My_elasticIP" {

tags = {
  Name= "MYEIP"
}
}
resource "aws_nat_gateway" "My_nat" {
  depends_on = [aws_eip.My_elasticIP]
  subnet_id = aws_subnet.public.id
  allocation_id = aws_eip.My_elasticIP.id
tags = {
    Name="My_Nat"
  }

}