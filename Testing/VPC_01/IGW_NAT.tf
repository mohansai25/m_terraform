resource "aws_internet_gateway" "My_internet" {
  vpc_id = aws_vpc.My_VPC.id
  tags = {
    Name="Test"
  }
}

resource "aws_eip" "My_elasticIP" {

tags = {
  Name= "MYEIP"
}
}
resource "aws_nat_gateway" "My_nat" {
  depends_on = [aws_eip.My_elasticIP]
  subnet_id = aws_subnet.public.id
  allocation_id = aws_eip.My_elasticIP.id
tags = {
    Name="My_Nat"
  }

}