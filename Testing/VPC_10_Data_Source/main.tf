provider "aws" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  #region = "${var.aws_region}"
  region = var.aws_region
}

data "aws_vpc" "testing"{
 id = var.vpcid
}
resource "aws_subnet" "public" {
  vpc_id = data.aws_vpc.testing.id
  cidr_block = "172.31.1.0/24"
  availability_zone = "us-east-2a"
  map_public_ip_on_launch = "true"
  #availability_zone = ""
  tags = {
   Name = "${var.public_subnet_Name}"
  }
}
