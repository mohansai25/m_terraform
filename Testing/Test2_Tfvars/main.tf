provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "${var.aws_region}"

}

resource "aws_vpc" "My_VPC" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags = {
    name="${var.vpc_name}"
  }
}
#resource "aws_subnet" "private" {
#  name= ""
#  vpc_id = aws_vpc.My_VPC.id
#  cidr_block = "${var.private_subnet_CIDR}"
#  availability_zone = ""
#  tags = {
#    name = "${var.private_subnet_Name}"
#  }
#}
#
#resource "aws_subnet" "public" {
#  vpc_id = aws_vpc.My_VPC.id
#  map_public_ip_on_launch = true
#  cidr_block = "${var.public_subnet_CIDR}"
#  tags = {
#    name = "${var.public_subnet_Name}"
#  }
#}
#
#
#resource "aws_internet_gateway" "My_internet" {
#  vpc_id = aws_vpc.My_VPC.id
#  tags = {
#    name="Test"
#  }
#}
#resource "aws_eip" "My_elasticIP" {
#
#tags = {
#  name= "MYEIP"
#}
#}
#
#resource "aws_nat_gateway" "My_nat" {
#  subnet_id = aws_subnet.public.id
#  allocation_id = aws_eip.My_elasticIP.id
#tags = {
#    name="My_Nat"
#  }
#
#}