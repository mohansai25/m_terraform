resource "aws_vpc" "My_VPC" {

  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags = {
    Name="${var.vpc_name}"
  }
}
resource "aws_subnet" "public" {
  vpc_id = aws_vpc.My_VPC.id
  cidr_block = "${var.public_subnet_CIDR}"
  map_public_ip_on_launch = "true"
  #availability_zone = ""
  tags = {
   Name = "${var.public_subnet_Name}"
  }
}

resource "aws_subnet" "private" {
  count=length(var.Private_Cidrs)
  vpc_id = aws_vpc.My_VPC.id
  availability_zone = element(var.Private_azs, count.index)
  cidr_block = element(var.Private_Cidrs,count.index )
  map_public_ip_on_launch = true
  #cidr_block = "${var.public_subnet_CIDR}"
  tags = {
    Name = "Private-${count.index}"
  }
}