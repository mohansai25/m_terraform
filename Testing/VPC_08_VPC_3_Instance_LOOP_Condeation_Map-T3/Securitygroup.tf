resource "aws_security_group" "Allow_All" {
  name        = "Ansible_allow_all"
  vpc_id      = aws_vpc.My_VPC.id // not relavent

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {

    Name = "Ansible_public_IGW"
  }
}
