
resource "aws_s3_bucket" "my_bucket" {
  bucket = "tesingbucket15"
  tags = {
    Name= "terraform_logs"
    Env ="Dev"
  }
}
resource "aws_s3_bucket_acl" "bucketacl" {
  depends_on = [aws_s3_bucket.my_bucket]
  bucket = aws_s3_bucket.my_bucket.id
  acl="private"
}


resource "aws_flow_log" "vpc_flow_log" {
  depends_on = [aws_s3_bucket.my_bucket]
  log_destination = aws_s3_bucket.my_bucket.arn
  log_destination_type = "s3"
  traffic_type = "ALL"
  vpc_id = aws_vpc.My_VPC.id
  tags = {
    Name="All VPC_LOGS"
  }

}
