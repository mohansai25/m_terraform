terraform {
backend "s3" {
  bucket = "mstat-lockfile"
  key = "devopsB16.tfstate"
  region = "us-east-1"
  dynamodb_table = "terraform-state-lock-dynamo"
}

}