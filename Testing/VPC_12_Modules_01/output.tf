output "vpc" {
  value = module.devops_testing_VPC.vpc_id
}

output "My_subnet" {
  value = module.devops_testing_VPC.subnet_public
}

output "SGW" {
  value = module.devops_testing_VPC.SGW
}