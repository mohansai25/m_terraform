resource "aws_subnet" "public" {
  depends_on = [aws_vpc.My_VPC]
  vpc_id = aws_vpc.My_VPC.id
  cidr_block = "${var.public_subnet_CIDR}"
  map_public_ip_on_launch = "true"
  #availability_zone = ""
  tags = {
   Name = "${var.public_subnet_Name}"
  }

}