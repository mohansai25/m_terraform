output "vpc_id" {
  value = aws_vpc.My_VPC.id
}

output "subnet_public" {
  value = aws_subnet.public.id
}
output "SGW" {
  value = aws_security_group.Allow_All.id
}