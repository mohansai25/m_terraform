resource "aws_instance" "public" {
  ami = var.image_Name
  instance_type = "t2.micro"
  subnet_id =var.pubsub_id
  associate_public_ip_address = true
  key_name = "mohansai_k8s"
  vpc_security_group_ids = [var.sgw]
  #user_data = "${file("install.sh")}"
  #user_data = "${"../../dev/install.sh"}"
  #user_data = "${file("install.sh")}" // we have to keep *.sh file in dev from where we are running the terraform
  #instance_initiated_shutdown_behavior = ""
  tags = {
    Name = "Red_Public"
    #Env=var.Env-Prod

  }
}
