resource "aws_instance" "private" {

  count=3
  ami = "ami-0149b2da6ceec4bb0"
  instance_type = "t2.micro"
  #subnet_id     =  "aws_subnet.publicd"
  subnet_id ="${element(aws_subnet.private.*.id,count.index)}"
  associate_public_ip_address = true
  key_name = "mohansai"

  vpc_security_group_ids = [aws_security_group.Allow_All.id]
  #user_data = "${file("install.sh")}"
  #user_data = "${"../../dev/install.sh"}"
  #instance_initiated_shutdown_behavior = ""
  tags = {
    Name = "Server-${count.index+1}"
  }
}
# subnet_id = element(aws_subnet.private.*.id,count.index )