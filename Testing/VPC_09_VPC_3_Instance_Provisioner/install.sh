#!/bin/bash
sudo apt-get update
sudo apt install openjdk-11-jdk -y
java -version
wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
echo deb https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install jenkins -y
#sudo systemctl status jenkins
#sudo ufw allow 8080
#echo "y" | sudo ufw enable
#cat "/var/lib/jenkins/secrets/initialAdminPassword"