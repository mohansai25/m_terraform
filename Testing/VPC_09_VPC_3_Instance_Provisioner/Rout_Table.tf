#
resource "aws_route_table" "public_route" {
  depends_on = [aws_internet_gateway.My_internet]
  vpc_id = aws_vpc.My_VPC.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.My_internet.id
  }
  tags = {
    Name= "Public"
  }
}
#resource "aws_route_table_association" "public_route_association" {
#  depends_on = [aws_route_table.public_route,aws_subnet.public,aws_internet_gateway.My_internet]
#  route_table_id = aws_route_table.public_route.id
#  subnet_id = aws_subnet.public.id
#}
#
resource "aws_route_table" "private_route" {
  depends_on = [aws_nat_gateway.My_nat]
  vpc_id = aws_vpc.My_VPC.id
   route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.My_nat.id
  }
  tags = {
    Name= "Private  "
  }
}
#resource "aws_route_table_association" "private_route_association" {
#  depends_on = [aws_subnet.private]
#  count = length(var.Private_azs)
#  route_table_id = aws_route_table.private_route.id
#  #subnet_id = aws_subnet.private.id
#  subnet_id = element(aws_subnet.private.*.id,count.index )
#}
##
