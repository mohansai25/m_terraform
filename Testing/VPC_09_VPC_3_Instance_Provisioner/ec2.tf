#resource "aws_instance" "private" {
#  count = "${var.Env-Prod=="Prod" ? 3 :1 }"
#  #count=3
#  #Look up using
#  ami = lookup(var.amis,var.aws_region )#"ami-0149b2da6ceec4bb0"
#  instance_type = "t2.micro"
#  #subnet_id     =  "aws_subnet.publicd"
#  subnet_id ="${element(aws_subnet.private.*.id,count.index)}"
#  associate_public_ip_address = true
#  key_name = "mohansai"
#
#  vpc_security_group_ids = [aws_security_group.Allow_All.id]
#  #user_data = "${file("install.sh")}"
#  #user_data = "${"../../dev/install.sh"}"
#  user_data = "${file("install.sh")}" // we have to keep *.sh file in dev from where we are running the terraform
#  #instance_initiated_shutdown_behavior = ""
#  tags = {
#    Name = "Server-${count.index+1}"
#    Env=var.Env-Prod
#  }
#  #
#  lifecycle {
#    create_before_destroy = true # Delete and create
#    #prevent_destroy = true # This instance cannot be destroyed IMP
#
#  }
#}
# subnet_id = element(aws_subnet.private.*.id,count.index )

resource "aws_instance" "public" {
  ami = var.image_Name
  instance_type = "t2.micro"
  subnet_id =var.pubsub_id
  associate_public_ip_address = true
  key_name = "mohansai"
  vpc_security_group_ids = [sgw]
  #user_data = "${file("install.sh")}"
  #user_data = "${"../../dev/install.sh"}"
  user_data = "${file("install.sh")}" // we have to keep *.sh file in dev from where we are running the terraform
  #instance_initiated_shutdown_behavior = ""
  tags = {
    Name = "Red_Public"
    Env=var.Env-Prod

  }
  #
#  lifecycle {
#    create_before_destroy = true
#    prevent_destroy = true
#  }


}
#variable "vpc_id" {}
#variable "pubsub_id" {}
#variable "sgw" {}
#variable "image_Name" {}