#variable "aws_access_key" {}
#variable "aws_secret_key" {}
variable "aws_region" {}
variable "vpc_cidr" {}
variable "vpc_name" {}

#variable "private_subnet_CIDR" {}
#variable "private_subnet_Name" {}
variable "public_subnet_CIDR"  {}
variable "public_subnet_Name"{}
variable "Env-Prod"{}

variable "Private_azs" {
  type = list(string)
  default = ["us-east-1a","us-east-1b","us-east-1c"]
}

variable "Private_Cidrs" {
  type = list(string)
  default = ["10.60.1.0/24","10.60.2.0/24","10.60.3.0/24","10.60.4.0/24","10.60.6.0/24"]
}

# uisng Map
variable "amis" {
  default = {
    us-east-1= "ami-0149b2da6ceec4bb0"
    us-east-2 = "ami-097a2df4ac947655f"
  }
}