
aws_region = "us-east-1"
vpc_cidr = "10.61.0.0/16"
vpc_name="ReadhotVPC"

private_subnet_CIDR="10.61.1.0/24"
private_subnet_Name="Private"

public_subnet_CIDR="10.61.2.0/24"
public_subnet_Name="Public"