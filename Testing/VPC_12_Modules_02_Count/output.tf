output "vpc" {
  value = module.devops_testing_VPC.vpc_id
}

output "My_subnet" {
  value = module.devops_testing_VPC.subnet_public
}

output "SGW" {
  value = module.devops_testing_VPC.SGW
}
output "vpc_private" {
  value = module.devops_testing_VPC.all_private
}

output "ec2" {
  value = module.devops_testing_Ec2.my_privare_list
}
