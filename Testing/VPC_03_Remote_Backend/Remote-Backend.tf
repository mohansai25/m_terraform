terraform {
backend "s3" {
  bucket = "testinf_statefile"
  key = "devopsB16.tfstate"
  region = var.aws_region
}

}