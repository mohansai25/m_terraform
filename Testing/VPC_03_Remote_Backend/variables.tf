#variable "aws_access_key" {}
#variable "aws_secret_key" {}
variable "aws_region" {}
variable "vpc_cidr" {}
variable "vpc_name" {}

variable "private_subnet_CIDR" {}
variable "private_subnet_Name" {}
variable "public_subnet_CIDR"  {}
variable "public_subnet_Name"{}
variable "azs" {
description = ""
  type = list(string)
  default = ["us-east-1a","us-east-1b","us-east-1b"]
}
